import React from 'react'
import Bill from './Bill'
import SeatList from './SeatList'
import styles from './Movie.module.scss'
import data from './data.json'
import classNames from 'classnames/bind'

const cx = classNames.bind(styles)
const Movie = () => {
    return (
        <div className={cx('wrapper')}>
            <div className={cx('body', 'grid grid-cols-3 gap-4')}>
                <SeatList data={data} />
                <Bill />
            </div>
        </div >
    )
}

export default Movie