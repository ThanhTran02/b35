import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BookingAction } from '../store/BTBooking/slice'
const Bill = () => {
    const dispatch = useDispatch()
    const { chooseSeats } = useSelector((state) => state.btBooking)

    return (
        <div className='text-white mt-14 '>
            <h1 className='text-2xl font-bold mb-5'> Danh sách ghế bạn đã chọn</h1>
            <div className='flex items-center '>
                <div className='text-4xl text-amber-600'>
                    <i class="fa fa-square"></i>
                </div>
                <p className='font-bold ml-5'>Ghế đã đặt</p>
            </div>
            <div className='flex items-center' >
                <div className='text-4xl text-green-600'>
                    <i class="fa fa-square"></i>
                </div>
                <p className=' font-bold ml-5'>Ghế đang chọn</p>
            </div>
            <div className='flex items-center'>
                <div className='text-4xl'>
                    <i class="fa fa-square"></i>
                </div>
                <p className='font-bold ml-5'>Ghế chưa đặt</p>
            </div>
            {/* tinhtien */}
            <table className="table-auto border-separate border border-slate-400 w-4/5 mt-5">
                <thead>
                    <tr>
                        <th className="border border-slate-300 ">Số ghế</th>
                        <th className="border border-slate-300 ">Giá</th>
                        <th className="border border-slate-300 ">Huỷ</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        chooseSeats.map((seat) => (
                            <tr key={seat.soGhe} >
                                <td className='border-separate border border-slate-400'>{seat.soGhe}</td>
                                <td className='border-separate border border-slate-400'>{seat.gia}</td>
                                <td className='border-separate border border-slate-400'>
                                    <button type="button" class=" bg-red-400 hover:bg-red-600 text-white  py-1 px-3 rounded" onClick={() => dispatch(BookingAction.setSeat(seat))}> Huỷ</button>
                                </td>
                            </tr>
                        ))
                    }
                    <tr>
                        <td className='border-separate border border-slate-400'>Tổng tiền</td>
                        <td className='border-separate border border-slate-400'>{chooseSeats.reduce((total, seat) => total += seat.gia, 0)}$</td>

                    </tr>
                </tbody>
            </table>
            <div className=' flex mt-5 items-center'>
                <p className='font-bold text-xl'>Thanh toán</p>
                <button class="ml-10 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center" onClick={
                    () => dispatch(BookingAction.setSeatBooked())
                }>
                    <span>Thanh toán</span>
                </button>
            </div>
        </div >
    )
}

export default Bill