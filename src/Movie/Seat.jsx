import React from 'react'
import styles from './Movie.module.scss'
import classNames from 'classnames/bind'
import { useDispatch, useSelector } from 'react-redux'
import { BookingAction } from '../store/BTBooking/slice'
const cx = classNames.bind(styles)
const Seat = ({ data }) => {
    const dispatch = useDispatch()
    const { chooseSeats, chooseSeated } = useSelector((state) => state.btBooking)
    return (
        <button className={cx('ghe', {
            booking: chooseSeats.find(e => e.soGhe === data.soGhe),
            booked: chooseSeated.find(e => e.soGhe === data.soGhe) || data.daDat
        })}
            onClick={() => dispatch(BookingAction.setSeat(data))}
        >
            {data.soGhe}
        </button >
    )
}

export default Seat