import React from 'react'
import Seat from './Seat'
import styles from './Movie.module.scss'
const SeatList = ({ data }) => {
    return (
        <div className='col-span-2 mt-10 '>
            <h1 className='text-amber-400 font-bold text-3xl '>Đặt vé xem phim</h1>
            <div className='grid justify-items-center mt-5'>
                <div className={styles.screen}>
                </div>
            </div>
            <div className='w-4/5 mt-5 m-auto '>
                {
                    data.map((rowSeat, index) => {
                        return (
                            <div className='flex  items-center gap-2' key={index}>
                                <p className="w-3 text-xl font-bold text-amber-600">{rowSeat.hang}</p>
                                <div className="flex">
                                    {
                                        rowSeat.danhSachGhe.map((seat) => (<Seat key={seat.soGhe} data={seat} />))
                                    }
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default SeatList