import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    chooseSeats: [],
    chooseSeated: [],
}

const BookingSlice = createSlice({
    name: 'BTBooking',
    initialState,
    reducers: {
        setSeat: (state, action) => {
            const index = state.chooseSeats.findIndex((seat) => seat.soGhe === action.payload.soGhe)
            if (index !== -1)
                state.chooseSeats.splice(index, 1)
            else
                state.chooseSeats.push(action.payload)
        },
        setSeatBooked: (state, action) => {
            state.chooseSeated = [...state.chooseSeated, ...state.chooseSeats]
            state.chooseSeats = []
        }
    }
});
export const { actions: BookingAction, reducer: Booking } = BookingSlice