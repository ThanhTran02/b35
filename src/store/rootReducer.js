import { combineReducers } from "redux";
import { Booking } from "./BTBooking/slice";

export const rootReducer = combineReducers({
    btBooking: Booking,
})